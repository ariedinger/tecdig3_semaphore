#include <stdio.h>          /* printf()                 */
#include <stdlib.h>         /* exit(), malloc(), free() */
#include <unistd.h>
#include <sys/types.h>      /* key_t, sem_t, pid_t      */
#include <sys/wait.h>       /* key_t, sem_t, pid_t      */
#include <sys/shm.h>        /* shmat(), IPC_RMID        */
#include <errno.h>          /* errno, ECHILD            */
#include <semaphore.h>      /* sem_open(), sem_destroy(), sem_wait().. */
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>          /* O_CREAT, O_EXEC          */
#include <time.h>

/* ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ */
// Defines
#define N     1E7  // Number of iterations

/* ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ */
// Function signatures
void credit(int n, int enableSemaphore, int numberOfChilds);
void debit(int n, int enableSemaphore, int numberOfChilds);
void updateBalance(int n, int add, int enableSemaphore);

/* ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ */
// Structures
struct shared
{
  sem_t sem;
  volatile int balance;
};

/* ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ */
// Shared memory
struct shared *p;
// Process ID
pid_t pid;
// Process status
int status;

/* ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ */
int main()
{
  /* ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ */
  // Local variables
  time_t t0, t1;
  double seg;
  char response[10];
  int enableSemaphore = 0;

  /* ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ */
  // Prompt the user for semaphore enablement
  printf("Enable semaphores? [Y/n]: ");
  fgets(response, sizeof(response), stdin);

  // Determine whether to enable semaphores based on user response
  if (response[0] == '\n' || response[0] == 'Y' || response[0] == 'y')
    enableSemaphore = 1;
  else
    enableSemaphore = 0;

  // Prompt the user for number of childs
  printf("Number of child processes?: ");
  int numberOfChilds;
  scanf("%d", &numberOfChilds);

  /* ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ */
  // Start the timer
  time(&t0);

  /* ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ */
  // Define a shared memory area where the semaphore will be located
  p = mmap(NULL, sizeof(*p), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);

  // Initialize the semaphore
  sem_init(&p->sem, 1, 1);

  // Set the initial balance to 0
  p->balance = 0;
  printf("\nInitial Balance = %d\n", p->balance);

  /* ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ */
  // Call the parent process
  credit(N, enableSemaphore, numberOfChilds);

  // Print the final balance
  printf("\nFinal Balance = %d\n", p->balance);

  // Destroy the semaphore
  sem_destroy(&p->sem);

  // Deallocate the shared memory area
  munmap(p, sizeof(*p));

  time(&t1);
  printf("Total time: %ld sec\n", t1 - t0);

  return 0;
}
/* ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ */
// Parent process
void credit(int n, int enableSemaphore, int numberOfChilds)
{
    // Start the parent process
    printf("\nParent process created: PID %d - sem: %p\n", getpid(), &p->sem);

    // Update the balance - the 1 argument indicates that the function
    // should process the sum of terms. Similar to +=
    updateBalance(n, 1, enableSemaphore);

    printf("\nCreating %d child processes.\n\n", numberOfChilds);

    debit(n,enableSemaphore, numberOfChilds);

    // Wait for all child processes to finish
    while ((pid = wait(&status)) > 0) {
      printf("Process %d has finished\n", pid);
    }
}

/* ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ */
// Child process
void debit(int n, int enableSemaphore, int numberOfChilds)
{
    // Create a fork of the program for each child process
    for (int i = 0; i < numberOfChilds; ++i) {
      if (fork() == 0) {
        // Start the child process
        printf("Child %d: PID %d - sem: %p\n", i, getpid(), &p->sem);

        // Update the balance - the -1 argument indicates that the function
        // should process the subtraction of terms. Similar to -=
        updateBalance(n/numberOfChilds, -1, enableSemaphore);
        exit(0);
      }
    }
}

/* ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ */
// Sum or subtract the balance, depending on whether it's being called
// by the parent or child process respectively
void updateBalance(int n, int add, int enableSemaphore)
{
    // For the number of iterations specified
    for (int i = 0; i < n; ++i)
    {
      // Semaphore wait if it is enabled
      if (enableSemaphore)
        sem_wait(&p->sem);

      // Add or subtract from the balance
      p->balance += add;

      // Semaphore post if it is enabled
      if (enableSemaphore)
        sem_post(&p->sem);
    }
}
